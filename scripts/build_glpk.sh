#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -eux

# Use a default of one core.
: "${NPROC:=1}"

# Download the specified version.
curl -L -O "ftp://ftp.gnu.org/gnu/glpk/glpk-${GLPK_VERSION}.tar.gz"
curl -L -O "ftp://ftp.gnu.org/gnu/glpk/glpk-${GLPK_VERSION}.tar.gz.sig"

# Verify the downloaded archive.
gpg --keyserver keys.gnupg.net --recv-keys 5981E818
gpg --verify "glpk-${GLPK_VERSION}.tar.gz.sig"

# Unpack and install GLPK.
tar -xzf "glpk-${GLPK_VERSION}.tar.gz"
cd "glpk-${GLPK_VERSION}"
./configure --disable-reentrant --with-gmp
make -j "${NPROC}" install
cd ..
rm -rf "glpk-${GLPK_VERSION}"*
