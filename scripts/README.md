## Build scripts

This directory contains the following content

* [`./build_pybind11.sh`](./build_pybind11.sh): script for downloading, building and
  installing specified pybind11
* [`./build_glpk.sh`](./build_glpk.sh): script for downloading, building and
  installing specified GLPK version
* [`./build_sundials.sh`](./build_sundials.sh): script for downloading, building
  and installing specified SUNDIALS version
* [`./jupyterlab_plotly.sh`](./jupyterlab_plotly.sh): script for building jupyterlab extensions
* [`./README.md`](./README.md): this document

