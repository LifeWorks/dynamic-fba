Source Files
============

.. _src: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src
.. _extension: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension
.. _`dfba_utils.cpp`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension/dfba_utils.cpp
.. _emblp: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension/emblp
.. _methods: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension/methods
.. _`solver_data.h`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension/solver_data.h
.. _`user_data.h`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/extension/user_data.h
.. _dfba: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba
.. _`control.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/control.py
.. _`exchange.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/exchange.py
.. _`helpers.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/helpers.py
.. _`jit.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/jit.py
.. _`model.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/model.py
.. _`library.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/library.py
.. _`variables.py`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/variables.py
.. _`plot`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/src/dfba/plot


Source files contained within the directory src_ are split between two
sub-directories separated by their language of implementation.

*C++*
-----

.. _Python: https://www.python.org/

The sub-directory extension_ contains the following content:

* `dfba_utils.cpp`_: contains source code for exposing the extension module to Python_
* emblp_: contains class and function declarations for embedded LP problems
* methods_: contains algorithms for integration of embedded LP problems
* `solver_data.h`_: struct exposed to Python_ for solver options
* `user_data.h`_: struct exposed to Python_ for model specification

*Python*
--------

The directory dfba_ contains the following content:

* `control.py`_: definition of class :class:`dfba.ControlParameter`
* `exchange.py`_: definition of class :class:`dfba.ExchangeFlux`
* `helpers.py`_: general helper functions
* `jit.py`_: tools for JIT compilation of dynamic library
* `model.py`_: definition of class :class:`dfba.DfbaModel`
* `library.py`_: methods for writing dynamic library
* `variables.py`_: definition of class :class:`dfba.KineticVariable`
* `plot`_: directory for additional visualization dependency
