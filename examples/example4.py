# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Aerobic growth of *S. cerevisiae* on glucose and xylose.

Organism -> Saccharomyces cerevisiae S288C
Model stored in http://bigg.ucsd.edu/models/iND750
"""

from os.path import dirname, join, pardir

from cobra.io import read_sbml_model

from dfba import DfbaModel, ExchangeFlux, KineticVariable


# DfbaModel instance initialized with cobra model
fba_model = read_sbml_model(
    join(dirname(__file__), pardir, "sbml-models", "iND750.xml.gz")
)
fba_model.solver = "glpk"
dfba_model = DfbaModel(fba_model)

# instances of KineticVariable
V = KineticVariable("Volume")
X = KineticVariable("Biomass")
Gluc = KineticVariable("Glucose")
Xyl = KineticVariable("Xylose")
Eth = KineticVariable("Ethanol")
Glyc = KineticVariable("Glycerol")

# add kinetic variables to dfba_model
dfba_model.add_kinetic_variables([V, X, Gluc, Xyl, Eth, Glyc])

# instances of ExchangeFlux
mu = ExchangeFlux("BIOMASS_SC4_bal")
v_G = ExchangeFlux("EX_glc__D_e")
v_Z = ExchangeFlux("EX_xyl__D_e")
v_E = ExchangeFlux("EX_etoh_e")
v_H = ExchangeFlux("EX_glyc_e")
v_O = ExchangeFlux("EX_o2_e")

# add exchange fluxes to dfba_model
dfba_model.add_exchange_fluxes([mu, v_G, v_Z, v_E, v_H, v_O])

# add rhs expressions for kinetic variables in dfba_model

Vgmax = 7.3
Kg = 1.026
Vzmax = 32.0
Kz = 14.85
Kig = 0.5
D = 0.035
Gin = 50.0
Zin = 50.0
Vomax = 8.0
Kie = 10

dfba_model.add_rhs_expression("Volume", D)
dfba_model.add_rhs_expression("Biomass", mu * X - D * X / V)
dfba_model.add_rhs_expression("Glucose", v_G * X + D * (Gin - Gluc) / V)
dfba_model.add_rhs_expression("Xylose", v_Z * X + D * (Zin - Xyl) / V)
dfba_model.add_rhs_expression("Ethanol", v_E * X - D * Eth / V)
dfba_model.add_rhs_expression("Glycerol", v_H * X - D * Glyc / V)

# add lower/upper bound expressions for exchange fluxes in dfba_model together
# with expression that must be non-negative for correct evaluation of bounds
dfba_model.add_exchange_flux_lb(
    "EX_glc__D_e", Vgmax * (Gluc / (Kg + Gluc)) * (1 / (1 + Eth / Kie)), Gluc
)
dfba_model.add_exchange_flux_lb(
    "EX_xyl__D_e",
    Vzmax * (Xyl / (Kz + Xyl)) * (1 / (1 + Eth / Kie)) * (1 / (1 + Gluc / Kig)),
    Xyl,
)
dfba_model.add_exchange_flux_lb("EX_o2_e", Vomax)

# add initial conditions for kinetic variables in dfba_model biomass (gDW/L),
# metabolites (g/L)
dfba_model.add_initial_conditions(
    {
        "Volume": 0.5,
        "Biomass": 0.05,
        "Glucose": 5.0,
        "Xylose": 5.0,
        "Ethanol": 0.0,
        "Glycerol": 0.0,
    }
)

# simulate model across interval t = [0.0,20.0](hours) with outputs for plotting
# every 0.1h
concentrations = dfba_model.simulate(0.0, 20.0, 0.1)
