# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""Test and add to the model the required variables to be simulated.

These variables correspond to those from example 1, as fixtures. This tests have
to be performed before the model tests and they modify the DfbaModel fixture to
accomodate to then be simulated.
TODO: should this set of tests be in test_unit?
"""


def test_add_variable(dfba_model, kinetic_variables):
    """Test for KineticVariable instantiation + adding to the model."""
    dfba_model.add_kinetic_variables(kinetic_variables)


def test_add_exchange_fluxes(dfba_model, exchange_reactions):
    """Test for ExchangeFlux instantiation + adding to the model."""
    dfba_model.add_exchange_fluxes(exchange_reactions)


def test_add_rhs_expression(dfba_model, kinetic_variables, exchange_reactions):
    """Test for ExchangeFlux instantiation + adding to the model."""
    dfba_model.add_kinetic_variables(kinetic_variables)
    dfba_model.add_exchange_fluxes(exchange_reactions)
    dfba_model.add_rhs_expression(
        "Biomass", kinetic_variables[0] * kinetic_variables[0]
    )
    dfba_model.add_rhs_expression(
        "Glucose",
        kinetic_variables[1] * 180.1559 * kinetic_variables[0] / 1000.0,
    )
    dfba_model.add_rhs_expression(
        "Xylose", kinetic_variables[2] * 150.13 * kinetic_variables[0] / 1000.0
    )
    dfba_model.add_rhs_expression(
        "Oxygen", kinetic_variables[3] * 16.0 * kinetic_variables[0] / 1000.0
    )
    dfba_model.add_rhs_expression(
        "Ethanol",
        kinetic_variables[4] * 46.06844 * kinetic_variables[0] / 1000.0,
    )


def test_add_exchange_flux_lb(dfba_model, kinetic_variables, exchange_reactions):
    """Test for Exchange bounds adding to the model."""
    dfba_model.add_kinetic_variables(kinetic_variables)
    dfba_model.add_exchange_fluxes(exchange_reactions)
    dfba_model.add_exchange_flux_lb(
        "EX_glc(e)",
        10.5
        * (kinetic_variables[1] / (0.0027 + kinetic_variables[1]))
        * (1 / (1 + kinetic_variables[1] / 20.0)),
        kinetic_variables[1],
    )
    dfba_model.add_exchange_flux_lb(
        "EX_o2(e)",
        15.0 * (kinetic_variables[3] / (0.024 + kinetic_variables[3])),
        kinetic_variables[3],
    )
    dfba_model.add_exchange_flux_lb(
        "EX_xyl_D(e)",
        6.0
        * (kinetic_variables[2] / (0.0165 + kinetic_variables[2]))
        * (1 / (1 + kinetic_variables[4] / 20.0))
        * (1 / (1 + kinetic_variables[1] / 0.005)),
        kinetic_variables[2],
    )


def test_add_initial_conditions(dfba_model, initials):
    """Set and test initial conditions."""
    dfba_model.add_initial_conditions(initials)
